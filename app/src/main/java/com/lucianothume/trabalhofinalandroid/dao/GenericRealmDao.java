package com.lucianothume.trabalhofinalandroid.dao;

import com.lucianothume.trabalhofinalandroid.model.ModelInterface;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

public abstract class GenericRealmDao<T extends RealmObject & ModelInterface> implements DaoInterface<T> {

    protected Class<T> type;

    protected GenericRealmDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public List<T> getAll() {
        List<T> list = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        list.addAll(realm.where(this.type).findAll());
        return list;
    }

    @Override
    public T get(int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(this.type).equalTo("id", id).findFirst();
    }

    @Override
    public int insert(final T item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                item.setId(getNextId());
                realm.insert(item);
            }
        });
        return item.getId();
    }

    @Override
    public boolean delete(int id) {
        final T item = this.get(id);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                item.deleteFromRealm();
            }
        });
        return true;
    }

    @Override
    public boolean update(final T item) {
        Realm rlm = Realm.getDefaultInstance();
        rlm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                T velho = realm.where(type).equalTo("id", item.getId()).findFirst();
                setUpdatedValues(velho, item);
                realm.insertOrUpdate(velho);
            }
        });
        return true;
    }

    protected int getNextId() {
        Realm realm = Realm.getDefaultInstance();
        Number max = realm.where(this.type).max("id");
        if (max == null) {
            return 1;
        } else {
            return max.intValue() + 1;
        }
    }

    abstract void setUpdatedValues(T velho, T novo);
}
