package com.lucianothume.trabalhofinalandroid.dao;

import com.lucianothume.trabalhofinalandroid.model.Settings;

public class SettingsDao extends GenericRealmDao<Settings> {

    private static SettingsDao singleton;

    public static SettingsDao getInstance(){
        if(singleton == null){
            singleton = new SettingsDao();
            if(singleton.get(1) == null){
                singleton.insert(Settings.getDefault());
            }
        }
        return singleton;
    }

    public SettingsDao(){
        super(Settings.class);
    }

    @Override
    void setUpdatedValues(Settings velho, Settings novo) {
        velho.setRestApiHost(novo.getRestApiHost());
        velho.setRestApiPath(novo.getRestApiPath());
        velho.setRestApiPort(novo.getRestApiPort());
    }
}
