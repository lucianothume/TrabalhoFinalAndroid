package com.lucianothume.trabalhofinalandroid.dao;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lucianothume.trabalhofinalandroid.model.Notification;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class NotificationDao extends GenericRestDao<Notification> {

    private static NotificationDao singleton;

    public static NotificationDao getInstance(){
        if(singleton == null){
            singleton = new NotificationDao();
        }
        return singleton;
    }


    public void getByUser(int userId, final onList<Notification> onList) {
        AsyncTask<Integer, Void, List<Notification>> task = new AsyncTask<Integer, Void, List<Notification>>() {
            @Override
            protected void onPostExecute(List<Notification> notifications) {
                onList.onData(notifications);
            }

            @Override
            protected List<Notification> doInBackground(Integer... integers) {
                return getByUser(integers[0]);
            }
        };
        task.execute(userId);
    }


    public List<Notification> getByUser(int userId) {
        String url = this.getBaseUrl() + "/list/?filters[userId]="+userId;
        return getList(url);
    }


    @Override
    protected String getBaseUrl() {
        return super.getBaseUrl() + "/notification";
    }

    @Override
    Type getType() {
        return new TypeToken<Notification>() {
        }.getType();
    }

    @Override
    Type getListType() {
        return new TypeToken<List<Notification>>() {
        }.getType();
    }
}
