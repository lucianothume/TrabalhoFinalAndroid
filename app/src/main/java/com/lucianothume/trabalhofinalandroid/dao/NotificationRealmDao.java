package com.lucianothume.trabalhofinalandroid.dao;

import com.lucianothume.trabalhofinalandroid.model.Notification;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class NotificationRealmDao extends GenericRealmDao<Notification> {

    private static NotificationRealmDao singleton = null;

    public static NotificationRealmDao getInstance() {
        if (singleton == null) {
            singleton = new NotificationRealmDao();
        }
        return singleton;
    }

    private NotificationRealmDao() {
        super(Notification.class);
    }

    public List<Notification> getByUser(int userId){
        List<Notification> byUser = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        byUser.addAll(realm.where(this.type).equalTo("userId", userId).findAll());
        return byUser;
    }

    @Override
    public List<Notification> getAll() {
        List<Notification> list = super.getAll();
        if(list.size() == 0){
            this.insertTests();
            return super.getAll();
        }
        return list;
    }

    @Override
    void setUpdatedValues(Notification velho, Notification novo) {

    }

    private void insertTests(){
        this.insert(new Notification(102, "Segunda Notificação", "Descrição da Segunda Notificação", "Endereço",2));
        this.insert(new Notification(104, "Quarta Notificação", "Descrição da Quarta Notificação", "Endereço",3));
        this.insert(new Notification(107, "Sétima Notificação", "Descrição da Sétima Notificação", "Endereço",4));
    }
}
