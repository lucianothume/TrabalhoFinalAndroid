package com.lucianothume.trabalhofinalandroid.dao;

import com.lucianothume.trabalhofinalandroid.model.ModelInterface;

import java.util.List;

public interface DaoInterface<T extends ModelInterface> {
    List<T> getAll();
    T get(int id);
    int insert(T item);
    boolean delete(int id);
    boolean update(T item);
}
