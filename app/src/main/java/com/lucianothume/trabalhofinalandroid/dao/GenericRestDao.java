package com.lucianothume.trabalhofinalandroid.dao;


import android.os.AsyncTask;

import com.lucianothume.trabalhofinalandroid.model.ModelInterface;

import java.net.MalformedURLException;
import java.util.List;

import com.google.gson.Gson;
import com.lucianothume.trabalhofinalandroid.model.Settings;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class GenericRestDao<T extends ModelInterface> implements DaoInterface<T> {


    public void getAll(final onList<T> onList) {
        AsyncTask<Void, Void, List<T>> task = new AsyncTask<Void, Void, List<T>>() {
            @Override
            protected List<T> doInBackground(Void... voids) {
                return getAll();
            }

            @Override
            protected void onPostExecute(List<T> ts) {
                if(onList != null) {
                    onList.onData(ts);
                }
            }
        };
        task.execute();
    }

    @Override
    public List<T> getAll() {
        String url = this.getBaseUrl() + "/list/";
        return getList(url);
    }

    protected List<T> getList(String url){
        HttpURLConnection conn = this.getConnection(url, "GET", false, true);
        if (conn == null) return null;
        try {
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }
            InputStream is = conn.getInputStream();
            String s = streamToString(is);
            List<T> list = (new Gson()).fromJson(s, getListType());
            is.close();
            conn.disconnect();
            return list;
        } catch (IOException e) {
            return null;
        }
    }

    public void get(int id, final onItem<T> onItem) {
        AsyncTask<Integer, Void, T> task = new AsyncTask<Integer, Void, T>() {
            @Override
            protected T doInBackground(Integer... integers) {
                return GenericRestDao.this.get(integers[0]);
            }

            @Override
            protected void onPostExecute(T t) {
                if(onItem != null) {
                    onItem.onData(t);
                }
            }
        };
        task.execute(id);
    }

    @Override
    public T get(int id) {
        String url = this.getBaseUrl() + "/" + id + "/";
        HttpURLConnection conn = this.getConnection(url, "GET", false, true);
        if (conn == null) return null;
        try {
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }
            InputStream is = conn.getInputStream();
            String s = streamToString(is);
            T item = (new Gson()).fromJson(s, getType());
            is.close();
            conn.disconnect();
            return item;
        } catch (IOException e) {
            return null;
        }
    }

    public void insert(T item, final onInsert<T> onInsert){
        AsyncTask<T, Void, Integer> task = new AsyncTask<T, Void, Integer>() {
            @Override
            protected Integer doInBackground(T[] ts) {
                return insert(ts[0]);
            }

            @Override
            protected void onPostExecute(Integer id) {
                if(onInsert != null) {
                    onInsert.onData(id);
                }
            }
        };
        task.execute(item);
    }

    @Override
    public int insert(T item) {
        String url = this.getBaseUrl() + "/";
        HttpURLConnection conn = this.getConnection(url, "POST", true, true);
        if (conn == null) return -1;
        String json = (new Gson()).toJson(item, getType());
        try {
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            os.close();
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return -1;
            }
            InputStream is = conn.getInputStream();
            String s = streamToString(is);
            T novo = (new Gson()).fromJson(s, getType());
            is.close();
            conn.disconnect();
            return novo.getId();
        } catch (IOException e) {
            return -1;
        }
    }

    public void delete(int id, final onDone onDone){
        AsyncTask<Integer, Void, Boolean> task = new AsyncTask<Integer, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Integer... integers) {
                return delete(integers[0]);
            }

            @Override
            protected void onPostExecute(Boolean ok) {
                if(onDone != null) {
                    onDone.onDone(ok);
                }
            }
        };
        task.execute(id);
    }

    @Override
    public boolean delete(int id) {
        String url = this.getBaseUrl() + "/" + id + "/";
        HttpURLConnection conn = this.getConnection(url, "DELETE", false, false);
        if (conn == null) return false;
        try {
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK && conn.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
                return false;
            }
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    @Override
    public boolean update(T item) {
        String url = this.getBaseUrl() + "/" + item.getId() + "/";
        HttpURLConnection conn = this.getConnection(url, "PUT", true, true);
        if (conn == null) return false;
        String json = (new Gson()).toJson(item, getType());
        try {
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            os.close();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK && conn.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
                return false;
            }
            InputStream is = conn.getInputStream();
            String s = streamToString(is);
            T updated = (new Gson()).fromJson(s, getType());
            is.close();
            conn.disconnect();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    protected HttpURLConnection getConnection(String url, String method, boolean doOutput, boolean doInput) {
        try {
            return this.getConnection(new URL(url), method, doOutput, doInput);
        } catch (MalformedURLException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    protected HttpURLConnection getConnection(URL urlCon, String method, boolean doOutput, boolean doInput) throws Exception {
        HttpURLConnection conn = (HttpURLConnection) urlCon.openConnection();
        conn.setReadTimeout(5000);
        conn.setConnectTimeout(5000);
        conn.setRequestMethod(method);
        conn.setDoInput(doInput);
        conn.setDoOutput(doOutput);
        if (doOutput) {
            conn.addRequestProperty("Content-Type", "application/json");
        }
        conn.connect();
        return conn;
    }

    protected String getBaseUrl() {
        Settings settings = SettingsDao.getInstance().get(1);
        return "http://" + settings.getRestApiHost() + ":" + settings.getRestApiPort() + "" + settings.getRestApiPath();
    }

    abstract Type getType();

    /**
     * Type listType = new TypeToken<List<T>>() {}.getType();
     *
     * @return
     */
    abstract Type getListType();

    protected String streamToString(InputStream is) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0) {
            stream.write(bytes, 0, lidos);
        }
        return new String(stream.toByteArray());
    }

    public interface onItem<T extends ModelInterface> {
        void onData(T item);
    }

    public interface onList<T extends ModelInterface> {
        void onData(List<T> list);
    }

    public interface onInsert<T extends ModelInterface> {
        void onData(int id);
    }

    public interface onDone {
        void onDone(boolean ok);
    }

}
