package com.lucianothume.trabalhofinalandroid;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.lucianothume.trabalhofinalandroid.model.Notification;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.NotificationViewHolder> {

    private List<Notification> notifications;
    private NotificationClick notificationClick;

    NotificationListAdapter(Context context, List<Notification> notifications) {
        this.notifications = notifications;
        if (context instanceof NotificationClick) {
            this.notificationClick = (NotificationClick) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement NotificationClick");
        }
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
        return new NotificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        Notification notification = notifications.get(position);
        holder.loadNotification(notification);
    }

    @Override
    public int getItemCount() {
        return notifications != null ? notifications.size() : 0;
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtListItemTitle)
        TextView txtTitle;

        @BindView(R.id.txtListItemDescription)
        TextView txtDescription;

        @BindView(R.id.txtListItemAddress)
        TextView txtAddress;

        @BindView(R.id.ListItemimageView)
        ImageView imageView;

        Unbinder unbinder;

        NotificationViewHolder(View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    int id = notifications.get(position).getId();
                    notificationClick.onNotificationClick(id);
                }
            });
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            unbinder.unbind();
        }

        public void loadNotification(Notification notification){
            txtTitle.setText(notification.getTitle());
            txtDescription.setText(notification.getDescription());
            txtAddress.setText(notification.getAddress());
            loadItemImage(notification);
        }

        public void loadItemImage(Notification notification) {
            Picasso.get()
                    .load(notification.getImageUrl())
                    .error(R.drawable.no_image)
                    .placeholder(R.drawable.loading)
                    .into(imageView);
        }
    }

    public interface NotificationClick {
        void onNotificationClick(int notificationId);
    }


}
