package com.lucianothume.trabalhofinalandroid;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.lucianothume.trabalhofinalandroid.controller.AuthController;
import com.lucianothume.trabalhofinalandroid.dao.GenericRestDao;
import com.lucianothume.trabalhofinalandroid.dao.NotificationDao;
import com.lucianothume.trabalhofinalandroid.dao.NotificationRealmDao;
import com.lucianothume.trabalhofinalandroid.model.Notification;
import com.lucianothume.trabalhofinalandroid.model.User;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class NotificationListFragment extends Fragment {

    private static final String ARG_LIST_TYPE = "listType";

    private NotificationListType listType;

    private AddNotificationClick addNotificationClick;
    private NotificationListAdapter notificationListAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    List<Notification> notifications;

    View rootView;

    Unbinder unbinder;

    public NotificationListFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param listType Tipo de lista.
     * @return A new instance of fragment NotificationListFragment.
     */
    public static NotificationListFragment newInstance(NotificationListType listType) {
        return newInstance(listType.ordinal());
    }

    public static NotificationListFragment newInstance(int listType) {
        NotificationListFragment fragment = new NotificationListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LIST_TYPE, listType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notifications = new ArrayList<>();
        if (getArguments() != null) {
            this.listType = NotificationListType.values()[getArguments().getInt(ARG_LIST_TYPE, 0)];
        }

        GenericRestDao.onList<Notification> onList = new GenericRestDao.onList<Notification>() {
            @Override
            public void onData(List<Notification> list) {
                if(list != null) {
                    notifications.addAll(list);
                    notificationListAdapter.notifyDataSetChanged();
                }
            }
        };
        notificationListAdapter = new NotificationListAdapter(getContext(), notifications);
        NotificationDao dao = NotificationDao.getInstance();
        User user = AuthController.getInstance().getAuthUser();
        if (user != null && this.listType == NotificationListType.MY_NOTIFICATIONS) {
            dao.getByUser(user.getId(), onList);
        } else {
            dao.getAll(onList);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notification_list, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        //RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(this.notificationListAdapter);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.floatingActionButton)
    public void fabClick(View view) {
        addNotificationClick.onAddNotificationClick();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddNotificationClick) {
            addNotificationClick = (AddNotificationClick) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AddNotificationClick");
        }
    }

    public NotificationListType getListType() {
        return listType;
    }

    public interface AddNotificationClick {
        void onAddNotificationClick();
    }

    public enum NotificationListType {
        MY_NOTIFICATIONS,
        IN_PROXIMITY
    }
}
