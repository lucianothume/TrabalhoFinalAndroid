package com.lucianothume.trabalhofinalandroid;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

public class TabsAdapter extends FragmentPagerAdapter {

    private Context ctx;

    public TabsAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        this.ctx = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        return NotificationListFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(Object object) {
        if(object instanceof NotificationListFragment) {
            NotificationListFragment listFragment = (NotificationListFragment) object;
            return listFragment.getListType().ordinal();
        } else {
            return PagerAdapter.POSITION_NONE;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Resources res = this.ctx.getResources();
        if(position == 0){
            return res.getString(R.string.my_notifications);
        } else {
            return res.getString(R.string.in_proximity);
        }
    }
}
