package com.lucianothume.trabalhofinalandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.lucianothume.trabalhofinalandroid.controller.AddNotificationController;
import com.lucianothume.trabalhofinalandroid.dao.GenericRestDao;
import com.lucianothume.trabalhofinalandroid.dao.NotificationDao;
import com.lucianothume.trabalhofinalandroid.model.Notification;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNotificationActivity extends AppCompatActivity implements SimpleDialog.SimpleDialogClickListener {

    static final int PLACE_PICKER_REQUEST = 1;

    @BindView(R.id.add_txtEndereco)
    EditText txtAddEndereco;

    @BindView(R.id.btnPlacePicker)
    Button btnPlacePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_notification_layout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnPlacePicker)
    protected void openPlacePicker(View view) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {

        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(this, "Google Play Services Not Available", Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.btnSave)
    protected void onSave(View view) {
        Notification notification = (new AddNotificationController()).getNotificationFromView(getWindow().getDecorView());
        NotificationDao.getInstance().insert(notification, new GenericRestDao.onInsert<Notification>() {

            @Override
            public void onData(int id) {
                finish();
            }
        });

    }

    @OnClick(R.id.btnCancel)
    protected void onCancel(View view) {
        SimpleDialog dialog = SimpleDialog.create(
                "Cancelar cadastro",
                "Deseja realmente cancelar o cadastro?",
                new int[]{R.string.no, R.string.yes}
        );
        dialog.openDialog(getSupportFragmentManager());
    }

    @Override
    public void onDialogClick(DialogInterface dialogInterface, int i) {
        if (i == -2) {
            this.finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place selectedPlace = PlacePicker.getPlace(this, data);
                txtAddEndereco.setText(selectedPlace.getAddress());
            }
        }
    }

}
