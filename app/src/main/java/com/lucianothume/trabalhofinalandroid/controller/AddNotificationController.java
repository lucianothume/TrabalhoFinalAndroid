package com.lucianothume.trabalhofinalandroid.controller;

import android.view.View;
import android.widget.EditText;

import com.lucianothume.trabalhofinalandroid.R;
import com.lucianothume.trabalhofinalandroid.model.Notification;

public class AddNotificationController {

    public Notification getNotificationFromView(View view){
        EditText txtTitulo = view.findViewById(R.id.add_txtTitulo);
        EditText txtDescricao = view.findViewById(R.id.add_txtDescricao);
        EditText txtEndereco = view.findViewById(R.id.add_txtEndereco);
        Notification notification = new Notification(0,
                txtTitulo.getText().toString(),
                txtDescricao.getText().toString(),
                txtEndereco.getText().toString(),
                AuthController.getInstance().getAuthUser().getId());
        return notification;
    }

}
