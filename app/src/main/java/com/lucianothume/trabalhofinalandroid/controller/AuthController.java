package com.lucianothume.trabalhofinalandroid.controller;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.lucianothume.trabalhofinalandroid.model.User;

public class AuthController {

    private static AuthController singleton;
    private FirebaseAuth mAuth;

    public static AuthController getInstance() {
        if (singleton == null) {
            singleton = new AuthController();
        }

        return singleton;
    }

    private AuthController() {
        mAuth = FirebaseAuth.getInstance();
    }

    public User getAuthUser() {
        return User.fromFirebase(mAuth.getCurrentUser());
    }

    public void login(String email, String senha, final LoginResult<User> loginResult) {
        mAuth.signInWithEmailAndPassword(email, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    loginResult.result(getAuthUser());
                } else {
                    loginResult.result(null);
                }
            }
        });
    }

    public void register(String email, String senha, final LoginResult<User> result) {
        mAuth.createUserWithEmailAndPassword(email, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    result.result(getAuthUser());
                } else {
                    result.result(null);
                }
            }
        });
    }

    public void logout() {
        mAuth.signOut();
    }

    public interface LoginResult<T> {
        void result(T result);
    }
}
