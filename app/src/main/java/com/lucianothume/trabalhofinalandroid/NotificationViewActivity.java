package com.lucianothume.trabalhofinalandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lucianothume.trabalhofinalandroid.controller.AuthController;
import com.lucianothume.trabalhofinalandroid.dao.GenericRestDao;
import com.lucianothume.trabalhofinalandroid.dao.NotificationDao;
import com.lucianothume.trabalhofinalandroid.dao.NotificationRealmDao;
import com.lucianothume.trabalhofinalandroid.model.Notification;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationViewActivity extends AppCompatActivity implements SimpleDialog.SimpleDialogClickListener {

    public static final String NOTIFICATION_ID = "notificationId";
    private Notification notification = null;

    @BindView(R.id.txtTitulo)
    TextView txtTitulo;

    @BindView(R.id.txtDescricao)
    TextView txtDescricao;

    @BindView(R.id.txtEndereco)
    TextView txtEndereco;

    @BindView(R.id.notificationImage)
    ImageView imageView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notification_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_back:
                onBackPressed();
                break;
            case R.id.menu_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, notification.toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.menu_delete:
                int userId = AuthController.getInstance().getAuthUser().getId();
                if(userId == notification.getUserId()) {
                    SimpleDialog dialog = SimpleDialog.create(
                            "Excluir registro",
                            "Deseja realmente excluir o registro?",
                            new int[]{R.string.no, R.string.yes}
                    );
                    dialog.openDialog(getSupportFragmentManager());
                } else {
                    SimpleDialog dialog = SimpleDialog.create(
                            "Excluir registro",
                            "Não é posível excluir a notificação de outro usuario!",
                            new int[]{R.string.ok}
                    );
                    dialog.setOnClickListener(null);
                    dialog.openDialog(getSupportFragmentManager());
                }
            default:
                return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_view);

        Toolbar toolbar = findViewById(R.id.return_view_toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        int notificationId = getIntent().getIntExtra(NOTIFICATION_ID, 0);
        NotificationDao.getInstance().get(notificationId, new GenericRestDao.onItem<Notification>() {
            @Override
            public void onData(Notification item) {
                notification = item;
                if (notification == null) {
                    Toast.makeText(NotificationViewActivity.this, "Notificação Inválida", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    NotificationViewActivity.this.getSupportActionBar().setTitle(notification.getTitle());

                    txtTitulo.setText(notification.getTitle());
                    txtDescricao.setText(notification.getDescription());
                    txtEndereco.setText(notification.getAddress());
                    loadItemImage();
                }
            }
        });

    }

    @OnClick(R.id.txtEndereco)
    public void addressClick(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        i.putExtra(MapsActivity.MAPS_ADDRESS, notification.getAddress());
        startActivity(i);
    }

    @Override
    public void onDialogClick(DialogInterface dialogInterface, int i) {
        if (i == -2) {
            NotificationDao.getInstance().delete(notification.getId(), new GenericRestDao.onDone() {
                @Override
                public void onDone(boolean ok) {
                    finish();
                }
            });
        }
    }

    public void loadItemImage(){
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        Picasso.get()
                .load(notification.getImageUrl())
                .resize(width,height)
                .centerCrop()
                .error(R.drawable.no_image)
                .placeholder(R.drawable.loading)
                .into(imageView);
    }
}
