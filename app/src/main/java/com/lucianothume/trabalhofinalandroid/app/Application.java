package com.lucianothume.trabalhofinalandroid.app;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Application extends android.app.Application {

    public void onCreate() {
        super.onCreate();
        // Inicializa o Realm. Isso deve ser feito somente quando a aplicação inicia
        Realm.init(this);
        //define o nome do esquema, a versão, e como será a migração
        //a linha new RealmConfiguration.Builder().build(); cria por padrão um arquivo chamado "default.realm" localizado em Context.getFilesDir()
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded() //deleteRealmIfMigrationNeeded() -> se você mudar a versao do banco com essa linha você apaga todas as informações da versão anterior
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Override
    public void onTerminate() {
        //finaliza o Realm quando encerra a execução do app
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }
}
