package com.lucianothume.trabalhofinalandroid;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.lucianothume.trabalhofinalandroid.controller.AuthController;
import com.lucianothume.trabalhofinalandroid.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity
        extends
        AppCompatActivity
        implements
        NavigationView.OnNavigationItemSelectedListener,
        NotificationListAdapter.NotificationClick,
        NotificationListFragment.AddNotificationClick {

    private static final long DRAWER_CLOSE_DELAY_MS = 250;

    private static final int RESULT_ADDNOTIFICATION = 1;

    private final Handler mDrawerActionHandler = new Handler();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;


    @Override
    protected void onStart() {
        super.onStart();
        User user = AuthController.getInstance().getAuthUser();
        if(user == null){
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        } else {
            navigate(R.id.nav_notificacoes);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        navigationView.setNavigationItemSelectedListener(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

    }

    private void navigate(final int itemId) {
        switch (itemId) {
            case R.id.nav_notificacoes:
                setFragment(new TabsFragment());
                break;
            case R.id.nav_add_notificacao:
                //setFragment(new AddNotificationFragment());
                Intent i = new Intent(this, AddNotificationActivity.class);
                startActivityForResult(i, RESULT_ADDNOTIFICATION);
                break;
            case R.id.nav_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                break;
            case R.id.nav_logout:
                AuthController.getInstance().logout();
                finish();
                break;
            case R.id.nav_exit:
                finish();
                break;
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mainContent, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onNotificationClick(int notificationId) {
        //Toast.makeText(this,"onNotificationClick Id: " + notificationId,Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, NotificationViewActivity.class);
        i.putExtra(NotificationViewActivity.NOTIFICATION_ID, notificationId);
        startActivityForResult(i, RESULT_ADDNOTIFICATION);
    }

    @Override
    public void onAddNotificationClick() {
        //Toast.makeText(this,"onAddNotificationClick",Toast.LENGTH_SHORT).show();
        navigate(R.id.nav_add_notificacao);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem menuItem) {

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.support.v7.appcompat.R.id.home) {
            return mDrawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_ADDNOTIFICATION:
                this.navigate(R.id.nav_notificacoes);
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
}
