package com.lucianothume.trabalhofinalandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lucianothume.trabalhofinalandroid.controller.AddNotificationController;
import com.lucianothume.trabalhofinalandroid.dao.NotificationRealmDao;
import com.lucianothume.trabalhofinalandroid.model.Notification;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddNotificationFragment extends Fragment implements SimpleDialog.SimpleDialogClickListener {

    AddNotificationClose addNotificationClose;
    View rootView;

    Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddNotificationClose) {
            addNotificationClose = (AddNotificationClose) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_notification_layout, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSave)
    protected void onSave(View view) {
        Notification notification = (new AddNotificationController()).getNotificationFromView(rootView);
        NotificationRealmDao.getInstance().insert(notification);
        close();
    }

    @OnClick(R.id.btnCancel)
    protected void onCancel(View view) {
        SimpleDialog dialog = SimpleDialog.create(
                "Cancelar cadastro",
                "Deseja realmente cancelar o cadastro?",
                new int[]{R.string.no, R.string.yes}
        );
        dialog.openDialog(getFragmentManager());
    }

    protected void close() {
        if (addNotificationClose != null) {
            addNotificationClose.onAddNotificationClose();
        }
    }

    @Override
    public void onDialogClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this.getContext(), "Botão clicado " + i, Toast.LENGTH_SHORT).show();
        close();
    }


    public interface AddNotificationClose {
        void onAddNotificationClose();
    }
}
