package com.lucianothume.trabalhofinalandroid.model;

import com.lucianothume.trabalhofinalandroid.dao.SettingsDao;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Settings extends RealmObject implements ModelInterface {

    @PrimaryKey
    private int id;
    private String restApiHost;
    private Integer restApiPort;
    private String restApiPath;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getRestApiHost() {
        return restApiHost;
    }

    public void setRestApiHost(String restApiHost) {
        this.restApiHost = restApiHost;
    }

    public Integer getRestApiPort() {
        return restApiPort;
    }

    public void setRestApiPort(Integer restApiPort) {
        this.restApiPort = restApiPort;
    }

    public String getRestApiPath() {
        return restApiPath;
    }

    public void setRestApiPath(String restApiPath) {
        this.restApiPath = restApiPath;
    }

    /*---------------------------------------*/

    public static Settings getDefault() {
        Settings settings = new Settings();
        settings.setRestApiHost("35.199.116.207");
        settings.setRestApiPort(80);
        settings.setRestApiPath("/restapi");
        return settings;
    }

}
