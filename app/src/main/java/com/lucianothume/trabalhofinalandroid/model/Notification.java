package com.lucianothume.trabalhofinalandroid.model;

import java.lang.reflect.Array;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Notification extends RealmObject implements ModelInterface{

    @PrimaryKey
    private int id;
    private String title;
    private String description;
    private String address;

    private int userId;

    public Notification(int id, String title, String description, String address, int userId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.address = address;
        this.userId = userId;
    }

    public Notification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    private static String[] images = {
            "http://2.bp.blogspot.com/-5QVa4b_kC38/UvU3fbgHujI/AAAAAAAAILw/qFCm3X_idlY/s1600/SAM_2279.JPG",
            "http://s2.glbimg.com/wUDrx5HqbyOP0lh7byNgMcREVh4=/s.glbimg.com/jo/g1/f/original/2013/10/06/2013-10-06_09.06.18.jpg",
            "http://s2.glbimg.com/0zQb5x3W4BZB0pLQxqXpKmGyXQs=/620x465/s.glbimg.com/jo/g1/f/original/2016/01/07/pitanga2ok.jpg"
    };

    public String getImageUrl(){
        if(id > 0){
            int i = id % images.length;
            return images[i];
        }
        return null;
    }


}
