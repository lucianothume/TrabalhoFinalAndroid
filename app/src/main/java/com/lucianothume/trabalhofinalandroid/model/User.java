package com.lucianothume.trabalhofinalandroid.model;

import com.google.firebase.auth.FirebaseUser;

public class User implements ModelInterface {
    private int id;
    private String nome;
    private String email;

    public User(int id, String nome, String email) {
        this.id = id;
        this.nome = nome;
        this.email = email;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static User fromFirebase(FirebaseUser firebaseUser) {
        if (firebaseUser == null) return null;
        byte[] bytes = firebaseUser.getUid().getBytes();
        int id = getHashFromUid(bytes);
        String email = firebaseUser.getEmail();
        String nome = email;
        return new User(id, nome, email);
    }

    private static int getHashFromUid(byte[] bytes){
        int i = 0;
        for(byte b : bytes){
            i = i | (b & 1);
            i <<= 1;
        }
        return i;
    }
}
