package com.lucianothume.trabalhofinalandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.lucianothume.trabalhofinalandroid.dao.SettingsDao;
import com.lucianothume.trabalhofinalandroid.model.Settings;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.settings_txtApiHost)
    EditText txtApiHost;

    @BindView(R.id.settings_txtApiPort)
    EditText txtApiPort;

    @BindView(R.id.settings_txtApiPath)
    EditText txtApiPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        SettingsDao dao = SettingsDao.getInstance();
        Settings settings = dao.get(1);
        txtApiHost.setText(settings.getRestApiHost());
        txtApiPort.setText(settings.getRestApiPort().toString());
        txtApiPath.setText(settings.getRestApiPath());
    }

    @OnClick(R.id.settings_btnSave)
    public void onSave(View view){
        Settings settings = new Settings();
        settings.setId(1);
        settings.setRestApiHost(txtApiHost.getText().toString());
        settings.setRestApiPort(Integer.parseInt(txtApiPort.getText().toString()));
        settings.setRestApiPath(txtApiPath.getText().toString());
        SettingsDao dao = SettingsDao.getInstance();
        dao.update(settings);
        this.finish();
    }

    @OnClick(R.id.settings_btnCancel)
    public void onCancel(View view){
        this.finish();
    }
}
