package com.lucianothume.trabalhofinalandroid;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import java.io.Serializable;

public class SimpleDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String EXTRA_MESSAGE = "message";
    private static final String EXTRA_TITLE = "title";
    private static final String EXTRA_BUTTONS = "buttons";
    private static final String DIALOG_TAG = "SimpleDialog";

    SimpleDialogClickListener onClickListener;

    public static SimpleDialog create(String title, String message, int[] buttonTexts) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_MESSAGE, message);
        bundle.putIntArray(EXTRA_BUTTONS, buttonTexts);
        SimpleDialog dialog = new SimpleDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    public void openDialog(FragmentManager supportFragmentManager) {
        if (supportFragmentManager.findFragmentByTag(DIALOG_TAG) == null) {
            show(supportFragmentManager, DIALOG_TAG);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String title = getArguments().getString(EXTRA_TITLE);
        String message = getArguments().getString(EXTRA_MESSAGE);
        int[] buttons = getArguments().getIntArray(EXTRA_BUTTONS);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);

        switch (buttons.length) {
            case 3:
                alertDialogBuilder.setNeutralButton(buttons[2], this);
            case 2:
                alertDialogBuilder.setNegativeButton(buttons[1], this);
            case 1:
                alertDialogBuilder.setPositiveButton(buttons[0], this);
        }
        return alertDialogBuilder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SimpleDialogClickListener) {
            onClickListener = (SimpleDialogClickListener) context;
        }
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.onClickListener != null) {
            onClickListener.onDialogClick(dialogInterface, i);
        }
    }

    public interface SimpleDialogClickListener extends Serializable {
        void onDialogClick(DialogInterface dialogInterface, int i);
    }

    public void setOnClickListener(SimpleDialogClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
