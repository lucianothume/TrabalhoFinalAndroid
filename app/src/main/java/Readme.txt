Em qual classe usei o NavigationView?
MainActivity

Em qual layout inclui a barra superior?
activity_main.xml

Qual atividade é chamada a partir do NavigationView?
AddNotificationActivity

Qual atividade inclui o fragmento?
MainActivity

Qual classe representa o fragmento(item 2.2)?
TabsFragment

Quais tipos de fragmentos você usou?
fragmento simples dentro de um FragmentPagerAdapter
DialogFragment para confirmação de cancelamento do cadastro

Qual classe é responsável pelo DialogFragment?
AddNotificationActivity

Qual(is) classe(s) POJO você criou?
Notification, User

Qual CRUD você salvou no banco de dados?
Notification

Qual banco você usou?
Realm

O que foi mais difícil de implementar? Explique quais dificuldades foram encontradas.
Não encontrei grandes problemas na implementação.
Porém tive problemas no FragmentPagerAdapter que não recarregava os fragmentos internos quando o fragmento que contém o adapter era recarregado.
A solução foi trocar o FragmentManager. de 'getFragmentManager()' para 'getChildFragmentManager()'